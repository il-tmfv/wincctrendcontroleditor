﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCCTrendControlEditor.Model
{
    public static class TagProviderType
    {
        public const string Archive = "1";
        public const string Online = "2";
    }

    public static class TrendControlId
    {
        public const string ButtonNew = "415";
        public const string EditName = "402";
        public const string EditLabel = "422";
        public const string EditTag = "408";
        public const string ComboLineStyle = "413";
        public const string ComboDotStyle = "417";
        public const string EditLineWeight = "418";
        public const string EditDotWeight = "419";
        public const string ComboProviderType = "407";
    }

    public static class TrendDotStyleId
    {
        public const string None = "0";
        public const string Dot = "1";
        public const string Squares = "2";
        public const string Circles = "3";
    }

    public static class TrendLineStyleId
    {
        public const string Solid = "0";
        public const string Dashed = "1";
        public const string Dotted = "2";
        public const string DotDash = "3";
        public const string DashDotDot = "4";
    }
}
