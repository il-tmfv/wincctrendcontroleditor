﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using WinCCTrendControlEditor.ViewModel;
using Xceed.Wpf.Toolkit;

namespace WinCCTrendControlEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+"); 
            return regex.IsMatch(text);
        }

        private void LineWeightSpinner_Spin(object sender, Xceed.Wpf.Toolkit.SpinEventArgs e)
        {
            ButtonSpinner spinner = (ButtonSpinner)sender;
            TextBox txtBox = (TextBox)spinner.Content;

            int value = String.IsNullOrEmpty(txtBox.Text) ? 0 : Convert.ToInt32(txtBox.Text);
            if (e.Direction == SpinDirection.Increase)
                value++;
            else
                value--;

            if (value > 10)
                value = 10;

            if (value < 1)
                value = 1;

            txtBox.Text = value.ToString();
        }

    }
}