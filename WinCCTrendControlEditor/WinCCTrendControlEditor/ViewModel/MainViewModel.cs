﻿using GalaSoft.MvvmLight;
using AutoItX3Lib;
using GalaSoft.MvvmLight.Command;
using System.Windows.Media;


using WinCCTrendControlEditor.Model;
using System.ComponentModel;
using System.Text;
using System;
using System.Windows;namespace WinCCTrendControlEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private AutoItX3 au3;
        private BackgroundWorker backWorkerWrite;
        private BackgroundWorker backWorkerRead;
        private BackgroundWorker backWorkerPasteEnglish;
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            backWorkerWrite = new BackgroundWorker();
            backWorkerWrite.RunWorkerCompleted += backWorkerWrite_RunWorkerCompleted;
            backWorkerWrite.DoWork += backWorkerWrite_DoWork;

            backWorkerRead = new BackgroundWorker();
            backWorkerRead.DoWork += backWorkerRead_DoWork;
            backWorkerRead.RunWorkerCompleted += backWorkerRead_RunWorkerCompleted;

            backWorkerPasteEnglish = new BackgroundWorker();
            backWorkerPasteEnglish.RunWorkerCompleted += backWorkerPasteEnglish_RunWorkerCompleted;
            backWorkerPasteEnglish.DoWork += backWorkerPasteEnglish_DoWork;
        }

        void backWorkerPasteEnglish_DoWork(object sender, DoWorkEventArgs e)
        {
            if (au3 == null)
                au3 = new AutoItX3();
            WaitForWindow(e);
        }

        void backWorkerPasteEnglish_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                SetMessageError("Окно так и не было найдено!");
            else
                PasteEngLabelsFunc();
            Busy = false;
        }

        void backWorkerRead_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
 	        if (e.Cancelled)
                SetMessageError("Окно так и не было найдено!");
            else
                ReadTexts();
            Busy = false;
        }

        void backWorkerRead_DoWork(object sender, DoWorkEventArgs e)
        {
 	        if (au3 == null)
                au3 = new AutoItX3();
            WaitForWindow(e);
        }

        void backWorkerWrite_DoWork(object sender, DoWorkEventArgs e)
        {
            if (au3 == null)
                au3 = new AutoItX3();
            WaitForWindow(e);
        }
        
        void backWorkerWrite_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                SetMessageError("Окно так и не было найдено!");
            else
                ProcessTags();
            Busy = false;
        }

        private void PasteEngLabelsFunc()
        {
            SetMessageSuccess("Обработка новых label...");
            string[] tagAttributes;
            string[] lines = InputText.Split(new char[] { '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries);

            foreach (var line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    au3.ControlSetText(WindowName, "", TrendControlId.EditLabel, line.Trim());
                    au3.Send("{DOWN}");
                }
            }
            SetMessageSuccess("Всё готово!");
        }

        /// <summary>
        /// Set success message in status bar
        /// </summary>
        /// <param name="message">Message text</param>
        private void SetMessageSuccess(string message)
        {
            Message = message;
            MessageColor = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        /// <summary>
        /// Set error message in status bar
        /// </summary>
        /// <param name="message">Message text</param>
        private void SetMessageError(string message)
        {
            Message = message;
            MessageColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));
        }

        /// <summary>
        /// Function returns tag provider for tag.
        /// Based on if it has '\' in name or not.
        /// </summary>
        /// <param name="fullName">Archive & tagname pair</param>
        /// <returns>Tag provider type</returns>
        private string GetProviderFromFullName(string fullName)
        {
            if (fullName.Contains("\\"))
                return TagProviderType.Archive;
            else
                return TagProviderType.Online;
        }

        /// <summary>
        /// Function gets tagname form archive & tagname pair
        /// </summary>
        /// <param name="fullName">Archive & tagname pair</param>
        /// <returns>Tag name</returns>
        private string GetTagNameFromFullName(string fullName)
        {
            string[] tmp = fullName.Split('\\');
            if (tmp.Length > 1)
                return tmp[1];
            else
                return tmp[0];
        }

        /// <summary>
        /// Function waits for settings window in separate thread (bgWorker)
        /// </summary>
        /// <param name="e">Arguments for worker</param>
        private void WaitForWindow(DoWorkEventArgs e)
        {
            int res = au3.WinWaitActive((string)e.Argument, "", 10);
            if (res == 0)
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Читает тексты из контрола в буфер обмена
        /// </summary>
        private void ReadTexts()
        {
 	        StringBuilder res = new StringBuilder();
            string prevName = "", currentName = "*";
            string currentLabel = "";
            string currentTagName = "";

            while (prevName != currentName)
            {
                prevName = currentName;

                currentName = au3.ControlGetText(WindowName, "", TrendControlId.EditName);
                currentLabel = au3.ControlGetText(WindowName, "", TrendControlId.EditLabel);
                currentTagName = au3.ControlGetText(WindowName, "", TrendControlId.EditTag);

                res.Append(String.Format("{0}\t{1}\t{2}\r\n", currentName, currentTagName, currentLabel));

                au3.Send("{DOWN}");
            }

            Clipboard.SetText(res.ToString());
        }

        /// <summary>
        /// Function processes all tags from input
        /// </summary>
        private void ProcessTags()
        {
            SetMessageSuccess("Обработка тегов...");
            string[] tagAttributes;
            string[] lines = InputText.Split(new char[] { '\n', '\r' }, System.StringSplitOptions.RemoveEmptyEntries);
            
            foreach (var line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    tagAttributes = line.Split('\t');
                    if (tagAttributes.Length != 2)
                        continue;
                    ProcessTag(tagAttributes[0], tagAttributes[1]);
                }
            }
            SetMessageSuccess("Всё готово!");
        }

        /// <summary>
        /// Добавляет тег на тренд
        /// </summary>
        /// <param name="tagName">Полное имя тега (включая имя архива, если он нужен)</param>
        /// <param name="label">Описание тега</param>
        private void ProcessTag(string tagName, string label)
        {
            au3.ControlClick(WindowName, "", TrendControlId.ButtonNew);

            au3.ControlSetText(WindowName, "", TrendControlId.EditName, GetTagNameFromFullName(tagName).Replace(ExcludeFromTag, ""));
            au3.ControlSetText(WindowName, "", TrendControlId.EditLabel, label);

            au3.ControlSend(WindowName, "", TrendControlId.ComboProviderType, GetProviderFromFullName(tagName));
            au3.ControlSetText(WindowName, "", TrendControlId.EditTag, tagName);

            au3.ControlSend(WindowName, "", TrendControlId.ComboDotStyle, TrendDotStyleId.None); 
            au3.ControlSetText(WindowName, "", TrendControlId.EditLineWeight, LineWeightSetting);

            au3.ControlSend(WindowName, "", TrendControlId.ComboLineStyle, LineStyleSetting.ToString());
        }

        private RelayCommand _pasteEngLabels;
        /// <summary>
        /// Gets the PasteEngLabels.
        /// </summary>
        public RelayCommand PasteEngLabels
        {
            get
            {
                return _pasteEngLabels
                    ?? (_pasteEngLabels = new RelayCommand(
                    () =>
                    {
                        if (!PasteEngLabels.CanExecute(null))
                        {
                            return;
                        }
                        Busy = true;
                        backWorkerPasteEnglish.RunWorkerAsync(WindowName);
                    },
                    () => !string.IsNullOrEmpty(InputText)));
            }
        }

        private RelayCommand _buttonReadTexts;
        /// <summary>
        /// Gets the ButtonReadTexts.
        /// </summary>
        public RelayCommand ButtonReadTexts
        {
            get
            {
                return _buttonReadTexts
                    ?? (_buttonReadTexts = new RelayCommand(
                    () =>
                    {
                        if (!ButtonReadTexts.CanExecute(null))
                        {
                            return;
                        }

                        Busy = true;
                        backWorkerRead.RunWorkerAsync(WindowName);
                    },
                    () => true));
            }
        }

        /// <summary>
        /// The <see cref="ExcludeTagEnding" /> property's name.
        /// </summary>
        public const string ExcludeTagEndingPropertyName = "ExcludeFromTag";
        private string _excludeFromTag = "_GW.Anz";
        /// <summary>
        /// Sets and gets the ExcludeTagEnding property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string ExcludeFromTag
        {
            get
            {
                return _excludeFromTag;
            }

            set
            {
                if (_excludeFromTag == value)
                {
                    return;
                }

                _excludeFromTag = value;
                RaisePropertyChanged(ExcludeTagEndingPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="Busy" /> property's name.
        /// </summary>
        public const string BusyPropertyName = "Busy";
        private bool _busy = false;
        /// <summary>
        /// Sets and gets the Busy property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool Busy
        {
            get
            {
                return _busy;
            }

            set
            {
                if (_busy == value)
                {
                    return;
                }

                _busy = value;
                RaisePropertyChanged(BusyPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="LineStyleSetting" /> property's name.
        /// </summary>
        public const string LineStyleSettingPropertyName = "LineStyleSetting";
        private int _lineStyleSetting = 0;
        /// <summary>
        /// Sets and gets the LineStyleSetting property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public int LineStyleSetting
        {
            get
            {
                return _lineStyleSetting;
            }

            set
            {
                if (_lineStyleSetting == value)
                {
                    return;
                }

                _lineStyleSetting = value;
                RaisePropertyChanged(LineStyleSettingPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="LineWeightSetting" /> property's name.
        /// </summary>
        public const string LineWeightSettingPropertyName = "LineWeightSetting";
        private string _lineWeightSetting = "2";
        /// <summary>
        /// Sets and gets the LineWeightSetting property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string LineWeightSetting
        {
            get
            {
                return _lineWeightSetting;
            }

            set
            {
                if (_lineWeightSetting == value)
                {
                    return;
                }

                _lineWeightSetting = value;
                RaisePropertyChanged(LineWeightSettingPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="InputText" /> property's name.
        /// </summary>
        public const string InputTextPropertyName = "InputText";
        private string _inputText = "";
        /// <summary>
        /// Sets and gets the InputText property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string InputText
        {
            get
            {
                return _inputText;
            }

            set
            {
                if (_inputText == value)
                {
                    return;
                }

                _inputText = value;
                ButtonClick.RaiseCanExecuteChanged();
                PasteEngLabels.RaiseCanExecuteChanged();
                RaisePropertyChanged(InputTextPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="WindowName" /> property's name.
        /// </summary>
        public const string WindowNamePropertyName = "WindowName";
        private string _windowName = "Свойства: WinCC OnlineTrendControl";
        /// <summary>
        /// Sets and gets the WindowName property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WindowName
        {
            get
            {
                return _windowName;
            }

            set
            {
                if (_windowName == value)
                {
                    return;
                }

                _windowName = value;
                RaisePropertyChanged(WindowNamePropertyName);
            }
        }

        /// <summary>
        /// The <see cref="MessageColor" /> property's name.
        /// </summary>
        public const string MessageColorPropertyName = "MessageColor";
        private Brush _messageColor = new SolidColorBrush(Color.FromRgb(0, 0, 0));
        /// <summary>
        /// Sets and gets the MessageColor property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Brush MessageColor
        {
            get
            {
                return _messageColor;
            }

            set
            {
                if (_messageColor == value)
                {
                    return;
                }

                _messageColor = value;
                RaisePropertyChanged(MessageColorPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="Message" /> property's name.
        /// </summary>
        public const string MessagePropertyName = "Message";
        private string _message = "";
        /// <summary>
        /// Sets and gets the Message property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                if (_message == value)
                {
                    return;
                }

                _message = value;
                RaisePropertyChanged(MessagePropertyName);
            }
        }

        private RelayCommand _buttonClick;
        /// <summary>
        /// Gets the ButtonClick.
        /// </summary>
        public RelayCommand ButtonClick
        {
            get
            {
                return _buttonClick
                    ?? (_buttonClick = new RelayCommand(
                    () =>
                    {
                        if (!ButtonClick.CanExecute(null))
                        {
                            return;
                        }

                        Busy = true;
                        backWorkerWrite.RunWorkerAsync(WindowName);
                    },
                    () => !string.IsNullOrEmpty(InputText)));
            }
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}